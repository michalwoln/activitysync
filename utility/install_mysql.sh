#!/usr/bin/env bash
db_root_user=root
db_root_password=root # password you will be prompted to type int for root during installation
db_user=activitysync@%
db_password=activitysync123 # change me !!!
db_database=ActivitySync

sudo apt-get install mysql-server libmysqlclient-dev
sudo mysql_install_db
sudo mysql_secure_installation

echo "Creating database..."
mysql --user="$db_root_user" --password="$db_root_password" --execute="CREATE DATABASE $db_database CHARACTER SET UTF8;"
mysql --user="$db_root_user" --password="$db_root_password" --execute="CREATE USER $db_user IDENTIFIED BY '${db_password}';"
mysql --user="$db_root_user" --password="$db_root_password" --execute="GRANT ALL PRIVILEGES ON $db_database.* TO $db_user;"
mysql --user="$db_root_user" --password="$db_root_password" --execute="FLUSH PRIVILEGES;"

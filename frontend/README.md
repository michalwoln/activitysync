# activitysync-Frontend (Angular.js)

## Konfiguracja środowiska

Przedstawiony opis konfiguracji zakłada pracę na systemie Windows. W sytemie Linux kroki mogą się lekko różnić, ale nie powinny sprawić problemu.

##### Instalacja narzędzi
* node.js  [(link)][nodejs] + dodanie do zmiennej środowiskowej PATH, standardowo:
    > C:\Program Files (x86)\nodejs
* konsola git [(link)][git] [opcjonalne]
* bower, poleceniem:
    > npm install -g bower

##### Konfiguracja plików

W katalogu głównym, po zaimportowaniu projektu należy pobrać pakiety, które są niezbędne do pracy nad aplikacją poleceniami:

> npm install

> bower install

## Użycie 

##### Polecenia

* uruchomienie lokalnego serwera, który zapewnia automatyczne odświeżanie zapisanych zmian w plikach projektu
    > gulp serve
* uruchomienie testów jednostkowych
    > gulp test
* uruchomienie procesu optymalizacji kodu i utworzenie katalogu z wersją produkcyjną
    > gulp

## Lincencje

Projekt zbudowany jest na podstawie szkieletu [generator-gulp-angular][generator] dostępnego na stronie autora.

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

[nodejs]: <https://nodejs.org/en/download/>
[git]: <https://git-for-windows.github.io/>
[generator]: <https://github.com/Swiip/generator-gulp-angular>

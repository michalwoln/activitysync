(function() {
  'use strict';

  angular
    .module('activitysync')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end')

  }

})();

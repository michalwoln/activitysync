(function() {
  'use strict';

  angular
    .module('activitysync')
    .config(routerConfig);

  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })

      .state('createEvent', {
        url: '/createEvent',
        templateUrl: 'app/createEvent/createEvent.html',
        controller: 'CreateEventController',
        controllerAs: 'createEvent'
      })
          .state('editEvent', {
            url: '/editEvent/:eventID',
            templateUrl: 'app/editEvent/editEvent.html',
            controller: 'EditEventController',
            controllerAs: 'editEvent'
          })

          .state('event', {
            url: '/event/:eventID',
            templateUrl: 'app/event/event.html',
            controller: 'EventController',
            controllerAs: 'event'
          })

      .state('teams', {
        url: '/teams',
        templateUrl: 'app/teams/teams.html',
        controller: 'TeamsController',
        controllerAs: 'teams'
      })

          .state('team', {
            url: '/team/:teamID',
            templateUrl: 'app/team/team.html',
            controller: 'TeamController',
            controllerAs: 'team'
          })

      .state('teamsManager', {
        url: '/teamsManager',
        templateUrl: 'app/teamsManager/teamsManager.html',
        controller: 'TeamsManagerController',
        controllerAs: 'teamsManager'
      });

    $urlRouterProvider.otherwise('/');
  }

})();

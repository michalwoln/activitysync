(function() {
  'use strict';

  angular
    .module('activitysync')
    .config(config);

  /** @ngInject */
  function config($logProvider, $mdDateLocaleProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

    $mdDateLocaleProvider.formatDate = function(date) {
      return moment(date).format('YYYY-MM-DD');
    };
  }

})();

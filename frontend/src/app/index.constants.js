/* global moment:false */
(function() {
  'use strict';

  angular
    .module('activitysync')
    .constant('moment', moment);

})();

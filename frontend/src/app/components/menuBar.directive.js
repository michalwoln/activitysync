(function() {
    /*jshint unused:false*/

    'use strict';

    angular
    .module('activitysync')
    .directive("menuBar", menuBar);

    function menuBar($window)
    {
        return {
            link: function(scope, element, attrs) {
                element.on('click',function(event) {

                });
            },
            controller: function($scope, $http, $state) {

              $scope.go = function(state) {
                $state.go(state);
              }

            },
            template: '<div class="menu" ng-if="facebookStatus.logged">'+
                        '<ul>'+
                          '<li ng-click="go(\'home\')">Events</li>'+
                          '<li ng-click="go(\'createEvent\')">Create event</li>'+
                          '<li ng-click="go(\'teams\')">Teams</li>'+
                          '<li ng-click="go(\'teamsManager\')">Teams Manager</li>'+
                        '</ul>'+
                      '</div>'

        };
    }

})();

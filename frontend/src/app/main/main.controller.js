(function () {
    'use strict';

    angular
        .module('activitysync')
        .controller('MainController', MainController);

    function MainController($http, $scope, $state, moment) {

        $scope.password = '';
        $scope.grade = function() {
            var size = $scope.password.length;
            if (size > 8) {
                $scope.strength = 'strong';
            } else if (size > 3) {
                $scope.strength = 'medium';
            } else {
                $scope.strength = 'weak';
            }
        };

        $scope.events = [];
        $scope.disciplines = [{uniqueID: 1, name: "football"}, {uniqueID: 2, name: "basketball"}];


        var disciplines = [];
//         $http.get("/activitysync/rest/disciplines/all").then(function (result) {
//             $scope.disciplines = result.data;
//         });

        $scope.sportEvent = {
            disciplineID: 0,
            numberOfPlayers: "",
            since: moment(),
            to: moment().add(1, 'days'),
            freePlaces: "",
            address: "",
            level: ""
        };

        $scope.getEvents = function () {

            $http.post("/activitysync/rest/events/find", {
                disciplineID: $scope.sportEvent.disciplineID,
                since: moment($scope.sportEvent.since),
                to: moment($scope.sportEvent.to),
                address: $scope.sportEvent.address.toString() || null,
                numberOfPlayers: parseInt($scope.sportEvent.numberOfPlayers) || null,
                freePlaces: parseInt($scope.sportEvent.freePlaces) || null,
                level: $scope.sportEvent.level
            }).then(function (result) {
                console.log(result.data);
                $scope.events = result.data;
            });
        };

        $scope.openEvent = function (eventID) {
            $state.go('event', {eventID: eventID});
        };

        $scope.setNow = function () {
            $scope.sportEvent.since = moment();
        };
        $scope.setDayAgo = function () {
            $scope.sportEvent.since = moment().subtract(1, 'days');
        };
        $scope.setWeekAgo = function () {
            $scope.sportEvent.since = moment().subtract(1, 'weeks');
        };

        $scope.setDay = function () {
            $scope.sportEvent.to = moment().add(1, 'days');
        };
        $scope.setWeek = function () {
            $scope.sportEvent.to = moment().add(1, 'weeks');
        };
        $scope.setMonth = function () {
            $scope.sportEvent.to = moment().add(1, 'months');
        };


    }
})();
